/**
 * Đầu vào: 2 cạnh góc vuông edge1, edge2
 * Các bước xử lý:
 * Đầu ra: cạnh huyền
 */

var edge1 = 3;
var edge2 = 4;

var result = edge1 * edge1 + edge2 * edge2;
console.log("result", result);

result = Math.sqrt(result);
console.log("result", result);

/**
 * Bài 1
 * Đầu vào: số ngày làm day
 * Các bước xử lý:
 * Đầu ra: tiền lương
 */

var day = 28;

var salary = day * 100000;

console.log("Salary", salary);

/**
 * Bài 2
 * Đầu vào: 5 số thực num1, num2, num3, num4, num5
 * Các bước xử lý:
 * Đầu ra: giá trị trung bình của 5 số
 */

var num1 = 1;
var num2 = 2;
var num3 = 3;
var num4 = 4;
var num5 = 5;

var trung_binh = (num1 + num2 + num3 + num4 + num5) / 5;
console.log("trung_binh", trung_binh);

/**
 * Bài 3
 * Đầu vào: số tiền USD
 * Các bước xử lý:
 * Đầu ra: số tiền VNĐ
 */

var usd = 2;
var vnd = usd * 23500;
console.log("VNĐ", vnd);

/**
 * Bài 4
 * Đầu vào: dài a, rộng b
 * Các bước xử lý:
 * Đầu ra: diện tích, chu vi
 */

var a = 5;
var b = 4;
var dien_tich = a * b;
console.log("Diện tích", dien_tich);
var chu_vi = (a + b) * 2;
console.log("Chu vi", chu_vi);

/**
 * Bài 5
 * Đầu vào: 1 số có 2 chữ số number
 * Các bước xử lý:
 * Đầu ra: tổng 2 kí số
 */

var number = 56;
var don_vi = number % 10;
var chuc = (number - don_vi) / 10;
var sum = don_vi + chuc;
console.log("Tổng 2 kí số", sum);
